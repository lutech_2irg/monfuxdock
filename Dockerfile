FROM jboss-eap64-openshift:latest
USER root

RUN yum install wget nfs-utils -y &&  \
    mkdir -p /opt/eap/modules/org/eclipse/persistence/main && \
    cd /opt/eap/modules && \
    chown jboss:jboss -R org/eclipse/persistence/main/
    
USER jboss
RUN /opt/eap/bin/add-user.sh admin password01! --silent
#ADD java/module.xml /opt/eap/modules/system/layers/base/org/postgresql/main/
ADD postgresql-9.4.1208.jre6.jar /opt/eap/standalone/deployments/
ADD java/monitorFlussiWebColl.war /opt/eap/standalone/deployments/
ADD standalone.xml /opt/eap/standalone/configuration/
ADD module.xml /opt/eap/modules/org/eclipse/persistence/main/
ADD eclipselink-2.5.2.jar /opt/eap/modules/org/eclipse/persistence/main/
ADD eclipselink-2.5.2.jar.index /opt/eap/modules/org/eclipse/persistence/main/

USER root
RUN chown jboss:jboss /opt/eap/standalone/deployments/*
USER jboss    
    
EXPOSE 8080 9990 22
CMD ["/opt/eap/bin/standalone.sh","-bmanagement","0.0.0.0","-b","0.0.0.0","-c","standalone.xml"]
